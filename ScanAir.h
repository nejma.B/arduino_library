/*
 * ScanAir.h
 * File description:
 * ScanAir class definition
 */

#ifndef SCANAIR_H_
#define SCANAIR_H_

#include <Arduino.h>
#include <SPI.h>
#include <WiFi.h>

#include "src/Type.h"
#include "src/ISensor.h"

namespace sa {
    #define ERROR_CONNECT_WIFI -1
    #define ERROR_CONNECT_SERVER 1
    #define SUCCESS_CONNECT 0

    static char server[] = "http://scan_air.com";
};

class ScanAir
{
    private:
        WiFiClient _client;
        int _port = 80;
    public:
        ScanAir();
        ~ScanAir();

        ScanAir(const ScanAir &) = delete;
        ScanAir(ScanAir &&) = delete;

        ScanAir &operator=(const ScanAir &) = delete;
        ScanAir &operator=(ScanAir &&) = delete;

        int connect(char ssid[], char wifi_pwd[], char username[], char account_pwd[]);

        template<typename T, typename... Args>
        T &create(Args&& ...params)
        {
            static_assert(type::is_base_of<ISensor, T>::value);
            return T(_client, type::forward<Args>(params)...);
        }
};

#endif /* SCANAIR_H_ */