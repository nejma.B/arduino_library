/*
 * ScanAir.cpp
 * File description:
 * ScanAir class implementation
 */

#include "ScanAir.h"

ScanAir::ScanAir()
{

}

ScanAir::~ScanAir()
{

}

int ScanAir::connect(char ssid[], char wifi_pwd[], char username[], char account_pwd[])
{
    int status = WL_IDLE_STATUS;

    status = WiFi.begin(ssid, wifi_pwd);
    if (status != WL_CONNECTED) {
        return ERROR_CONNECT_WIFI;
    } else {
        if (_client.connect(sa::server, _port)) {
            // Authentificate to scan'air server
            return SUCCESS_CONNECT;
        }
        return ERROR_CONNECT_SERVER;
    }
}