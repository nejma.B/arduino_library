/*
 * Type.h
 * File description:
 * #include <type_traits> && #include <utility> equivalent
 * 
 * Arduino doesn't have C++11 type_traits and utility headers
 * so here is what is needed for ScanAir to work
 */

#ifndef TYPE_H_
#define TYPE_H_

namespace type
{
    // #include <utility>
    template<class T> struct remove_reference { typedef T type; };
    template<class T> struct remove_reference<T&> { typedef T type; };
    template<class T> struct remove_reference<T&&> { typedef T type; };

    template<class T>
    T&& forward (typename remove_reference<T>::type& t) noexcept
    {
        return static_cast<T&&>(t);
    };

    template<class T>
    T&& forward (typename remove_reference<T>::type&& t) noexcept
    {
        return static_cast<T&&>(t);
    };

    // #include <type_traits>
    template<class T, T v>
    struct integral_constant {
        static constexpr T value = v;
        using value_type = T;
        using type = integral_constant; // using injected-class-name
        constexpr operator value_type() const noexcept { return value; }
        constexpr value_type operator()() const noexcept { return value; } //since c++14
    };

    typedef integral_constant<bool, true> true_type;
    typedef integral_constant<bool, false> false_type;

    template<class _Tp1, class _Tp2>
    struct is_same : public false_type
    { };

    template<class _Tp>
    struct is_same<_Tp, _Tp> : public true_type
    { };

    template<class T>
    struct __uoc_aux
    {
        private:
            template<class _Up>
            static true_type __test(int _Up::* );

            template<class>
            static false_type __test(...);
        public:
            static const bool __value = is_same<decltype(__test<T>(0)), true_type>::value;
    };

    template<typename _Tp>
    struct is_union : public integral_constant<bool, __uoc_aux<_Tp>::__value>
    { };

    namespace detail {
        template<class T>
        integral_constant<bool, !is_union<T>::value> test(int T::*);

        template<class>
        false_type test(...);
    };

    template<class T>
    struct is_class : decltype(detail::test<T>(nullptr))
    { };

    namespace details {
        template <typename B>
        true_type  test_pre_ptr_convertible(const volatile B*);
        template <typename>
        false_type test_pre_ptr_convertible(const volatile void*);
 
        template <typename, typename>
        auto test_pre_is_base_of(...) -> true_type;
        template <typename B, typename D>
        auto test_pre_is_base_of(int) -> decltype(test_pre_ptr_convertible<B>(static_cast<D*>(nullptr)));
    };

    template<typename Base, typename Derived>
    struct is_base_of : integral_constant<bool, is_class<Base>::value && is_class<Derived>::value && decltype(details::test_pre_is_base_of<Base, Derived>(0))::value>
    { };
};

#endif /* TYPE_H_ */