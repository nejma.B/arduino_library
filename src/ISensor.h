/*
 * ISensor.h
 * File description:
 * ISensor interface definition
 */

#ifndef ISENSOR_H_
#define ISENSOR_H_

#include <Arduino.h>

class ISensor
{
    public:
        virtual ~ISensor() = default;

        virtual int sendDataToServer() = 0;
        virtual void compute() = 0;
};

#endif /* ISENSOR_H_ */